Задача №2.

Имеется строка:
https://www.somehost.com/test/index.html?param1=4&param2=3&param3=2&param4=1&param5=3
Напишите функцию, которая:
1) удалит параметры со значением “3”;
2) отсортирует параметры по значению;
3) добавит параметр url со значением из переданной ссылки без параметров (в примере: /test/index.html);
4) сформирует и вернёт валидный URL на корень указанного в ссылке хоста.
В указанном примере функцией должно быть возвращено:
https://www.somehost.com/?param4=1&param3=2&param1=4&url=%2Ftest%2Findex.html

<?php
$url = 'https://www.somehost.com/test/index.html?param1=4&param2=3&param3=2&param4=1&param5=3';


function handle(string $url) : string
{
    $url = parse_url($url);

    $params = [];

    parse_str($url['query'], $params);

    foreach ($params as &$param){
        if($param == 3) unset($param);
    }

    asort($params);

    $params['path'] = $url['path'];

    return sprintf('%s://%s%s?%s', $url['scheme'], $url['host'], $url['path'], http_build_query($params));

}

echo handle('https://www.somehost.com/test/index.html?param1=4&param2=3&param3=2&param4=1&param5=3');


