Занимались ли вы разработкой автоматизированных функциональных тестов, нагрузочных тестов, либо unit-тестов?
 Если да – сообщите, что доводилось использовать, какие решали задачи и с каким результатом. Если нет – не страшно, поможем освоить :)

 =====================================

 В общем-то да, занимался, но особых успехов не достиг. Уровень моего тестирования вы можете видеть в task4/tests.
