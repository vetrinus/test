Задача №1.

CREATE TABLE `users` (
`id` 		INT(11) NOT NULL AUTO_INCREMENT,
`name` 	VARCHAR(255) DEFAULT NULL,
`gender`	INT(11) NOT NULL COMMENT '0 - не указан, 1 - мужчина, 2 - женщина.',
`birth_date`	INT(11) NOT NULL COMMENT 'Дата в unixtime.',
PRIMARY KEY (`id`)
);
CREATE TABLE `phone_numbers` (
`id` 		INT(11) NOT NULL AUTO_INCREMENT,
`user_id`	INT(11) NOT NULL,
`phone`	VARCHAR(255) DEFAULT NULL,
PRIMARY KEY (`id`)
);

Оптимизируйте структуру таблиц и напишите запрос, возвращающий имена и количества телефонных номеров девушек в возрасте от 18 до 22 лет.

SELECT u.name, COUNT(p.id)
from users as u
INNER JOIN phone_numbers as p
ON p.user_id = u.id
WHERE FROM_UNIXTIME(birth_date)
BETWEEN
    CURRENT_DATE() - INTERVAL 22 YEAR
AND
    CURRENT_DATE() - INTERVAL 18 YEAR
AND u.gender = 2

GROUP BY p.id

Что касается оптимизации. Я вижу тут четыре момента.
1. unixtime для хранения даты рождения избыточен, т.к. в реальном мире значение имеет только дата, а не часы, минуты и секунды внутри этой даты.
Таким образом можно INT(11) заменить на date. Таким образом снизятся расходы на конвертацию unixtime.
2. INT(11) для хранения пола. Тут есть два варианта - nullable boolean, или enum.
3. Проставить в таблице с телефонами внешний ключ на таблицу пользователей.
4. Проставить индексы на gender & birth_date.