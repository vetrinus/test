<?php

namespace task3\interfaces;

/**
 * Interface IArticle
 * @package task3\interfaces
 */
interface IArticle
{
    /**
     * IArticle constructor.
     * @param IUser $author
     * @param string $content
     */
    public function __construct(IUser $author, string $content);

    /**
     * @param IUser $author
     * @return void
     */
    public function setAuthor(IUser $author) : void;

    /**
     * @return IUser
     */
    public function getAuthor() : IUser;


}