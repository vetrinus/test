<?php

namespace task3\interfaces;

/**
 * Interface IUser
 * @package task3\interfaces
 */
interface IUser
{
    /**
     * @return IArticle[]
     */
    public function getArticles() : array;

    /**
     * @param string $content
     * @return IArticle
     */
    public function createArticle(string $content): IArticle;
}