<?php
/**
 * Created by PhpStorm.
 * User: vetrinus
 * Date: 02.08.18
 * Time: 20:04
 */

namespace task3\entities;


use task3\interfaces\IArticle;
use task3\interfaces\IUser;

/**
 * Class Article
 * @package task3\entities
 */
class Article implements IArticle
{
    private $author;
    private $content;

    public function __construct(IUser $author, string $content)
    {
        $this->author = $author;
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * @return IUser
     */
    public function getAuthor(): IUser
    {
        return $this->author;
    }

    /**
     * @param IUser $author
     */
    public function setAuthor(IUser $author): void
    {
        $this->author = $author;
    }
}