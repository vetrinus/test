<?php
/**
 * Created by PhpStorm.
 * User: vetrinus
 * Date: 02.08.18
 * Time: 20:04
 */
namespace task3\entities;


use task3\interfaces\IArticle;
use task3\interfaces\IUser;

/**
 * Class User
 * @package task3\entities
 */
class User implements IUser
{
    /**
     * @param string $content
     * @return IArticle
     */
    public function createArticle(string $content): IArticle
    {
        return new Article($this, $content);
    }

    /**
     * @return IArticle[]
     */
    public function getArticles(): array
    {
        return [];
    }
}