<?php
/**
 * Created by PhpStorm.
 * User: vetrinus
 * Date: 02.08.18
 * Time: 20:19
 */

namespace task4\services;


use task4\Database;
use task4\forms\BatchUserSelectForm;

class UserService
{
    private $database;

    public function __construct(Database $database)
    {
        $this->database = $database;
    }

    public function getUsers(BatchUserSelectForm $form) : array
    {
        $users = $form->getUsers();
        $clause = implode(',', $users);
        $statement = "SELECT * from users WHERE ID IN ({$clause})";

        return $this->database->execute($statement);
    }
}