<?php
/**
 * Created by PhpStorm.
 * User: vetrinus
 * Date: 02.08.18
 * Time: 23:44
 */

namespace task4\tests\unit;


use task4\forms\BatchUserSelectForm;

class FormTest extends \Codeception\Test\Unit
{
    public function testLoadSuccess()
    {
        $form = new BatchUserSelectForm();
        $form->load('1,2,3,4');

        $this->assertTrue($form->isValid());
        $this->assertEquals([1,2,3,4], $form->getUsers());
    }

    public function testEmptyLoad()
    {
        $form = new BatchUserSelectForm();
        $this->expectExceptionMessage('List of users cannot be empty');
        $form->load('');
    }

    public function testNotValidLoad()
    {
        $form = new BatchUserSelectForm();
        $this->expectExceptionMessage('value must be an integer');
        $form->load('1, OR 1=1');
    }
}