<?php

namespace task4\tests;

use task4\Container;
use task4\Database;
use task4\services\UserService;

/**
 * Class ContainerTest
 * @package task4\tests
 * @property Container $container
 */
class ContainerTest extends \Codeception\Test\Unit
{

    private $container;

    public function _before()
    {
        $this->container = new Container();
    }

    public function testGetService()
    {
        $this->assertInstanceOf(UserService::class, $this->container->getService());
    }

    public function testGetDatabase()
    {
        $this->assertInstanceOf(Database::class, $this->container->getDatabase());
    }
}