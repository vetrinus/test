<?php
/**
 * Created by PhpStorm.
 * User: vetrinus
 * Date: 02.08.18
 * Time: 20:33
 */

namespace task4;


use task4\services\UserService;

class Container
{

    public function getDatabase() : Database
    {

        $params = require 'params/database.php';

        return new Database(
            $params['dsn'],
            $params['user'],
            $params['password']
        );
    }

    public function getService() : UserService
    {
        return new UserService($this->getDatabase());
    }
}