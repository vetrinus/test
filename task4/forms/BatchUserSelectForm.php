<?php
/**
 * Created by PhpStorm.
 * User: vetrinus
 * Date: 02.08.18
 * Time: 20:23
 */

namespace task4\forms;


use Webmozart\Assert\Assert;

/**
 * Class BatchUserSelectForm
 * @package task4\forms
 */
class BatchUserSelectForm
{
    private $users;

    /**
     * BatchUserSelectForm constructor.
     * @param string $list
     */
    public function load(string $list)
    {
        Assert::stringNotEmpty($list, 'List of users cannot be empty');

        $explodedList = explode(',', $list);

        Assert::notEmpty($explodedList, 'List of users cannot be empty');

        foreach ($explodedList as $item)
        {
            $this->assertItem($item);
        }

        $this->users = $explodedList;
    }

    /**
     * @return array
     */
    public function getUsers() : array
    {
        return $this->users;
    }

    /**
     * @return bool
     */
    public function isValid() : bool
    {
        return !empty($this->users);
    }

    /**
     * @param $item
     */
    private function assertItem($item)
    {
        Assert::numeric($item, 'value must be an integer');
        Assert::greaterThan($item, 0, 'User id cannot be less than 1');
    }
}