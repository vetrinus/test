<?php
/**
 * Created by PhpStorm.
 * User: vetrinus
 * Date: 02.08.18
 * Time: 20:15
 */

namespace task4;

/**
 * Class Database
 * @package task4
 */
class Database
{

    private $pdo;

    /**
     * Database constructor.
     * @param string $dsn
     * @param string $user
     * @param string $password
     */
    public function __construct(
        string $dsn,
        string $user,
        string $password
    )
    {
        $this->pdo = new \PDO($dsn, $user, $password);
    }


    public function execute(string $statement)
    {
        $result =  $this->pdo->query($statement, \PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
}