<?php

require '../../vendor/autoload.php';

$container = new \task4\Container();
$service = $container->getService();
$form = new \task4\forms\BatchUserSelectForm();

$form->load($_GET['user_ids']);

$users = $service->getUsers($form);

foreach ($users as $user){
    echo sprintf('<a href="show_user.php?id=%d">%s</a>', $user['id'], $user['name']);
}